$(function() {

$('.thumbnail img').css({
        'height': $('.thumbnail img').height()/2.5
    });

	var privateads =  {

		userLocation: window.location.href,

		enableAccess: function() {

			$("*").addClass("access");
			this.setAccess(true);


		},

		disableAccess: function() {

			$("*").removeClass("access");
			this.setAccess(false);

		},

		setAccess: function(enable) {
			if(enable == true) {
				localStorage.setItem("access", "true");
				$("#accessible").text("Click here to disable accessibility mode");
			} else {
				$("#accessible").text("Click here to enable accessibility mode");
				localStorage.removeItem("access");
			}
		},

		setStorage: function(item, value) {
			return localStorage.setItem(item, value);
		},

		getStorage: function(item) {
			return localStorage.getItem(item);
		},

		removeStorage: function(item) {
			return localStorage.removeItem(item);
		},

		setLocation: function() {

			

			if(this.userLocation.indexOf("submit") > -1) {
				$("#submit").addClass("active");
			}else if(this.userLocation.indexOf("contact") > -1) {
				$("#contact").addClass("active");
			}else if(this.userLocation.indexOf("about") > -1) {
				$("#about").addClass("active");
			}else if(this.userLocation.indexOf("help") > -1) {
				$("#help").addClass("active");
			} else {
				$("#home").addClass("active");
			}

		},

		registerSuccess: function(data) {

		if(data.success) {
			$("#registerForm").fadeOut();
			$("#registerConfirmation").fadeIn();

		}

		},

		loginSuccess: function(data) {

		if(data.success) {
			$("#loginForm").fadeOut();
			$("#loginConfirmation").fadeIn();

			var username = $("#loginUser").val();
			var password = $("#loginPass").val();

			privateads.setStorage("username", username);
			privateads.setStorage("password", password);
			privateads.setStorage("auth", true);

			setTimeout(function(){ window.location.reload() }, 3000);

		} else {
			$("#loginFail").fadeIn();
			setTimeout(function(){ $("#loginFail").hide(); }, 5000);
		}

		},

		submitSuccess: function(data) {

		if(data.success) {
			$("#submitAdForm").fadeOut();
			$("#submitSuccess").fadeIn();

		} else {
			$("#submitFail").fadeIn();
			setTimeout(function(){ $("#submitFail").hide(); }, 5000);
		}

		}


	}


	var access = privateads.getStorage("access");



	if(access) {
		privateads.enableAccess();
	}

	$("#accessible").click(function() {

		if(privateads.getStorage("access")) {
			privateads.disableAccess();
			return;
		} 

		privateads.enableAccess();


	});

	privateads.setLocation();

	$("#registerForm").ajaxForm({
		dataType:  'json',
		success: privateads.registerSuccess
	});


	$("#loginForm").ajaxForm({
		dataType:  'json',
		success: privateads.loginSuccess
	});

	$("#submitAdForm").ajaxForm({
		dataType:  'json',
		success: privateads.submitSuccess
	});

	$("#logoutLink").click(function() {

		privateads.removeStorage("username");
		privateads.removeStorage("password");
		privateads.removeStorage("auth");

		$.get("user/logout");

	    window.location.reload();

	});

	if(privateads.getStorage("auth")) {
		$("#userForm").hide();
		$("#authUserName").text("Logged in as: " + privateads.getStorage("username"));
		$("#logoutLink").show();
	}


});