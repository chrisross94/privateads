<?php

class Ad extends Eloquent {

	protected $guarded = array('id');

	public function scopeName($query, $name) {

		return $query->where(function($query) use ($name) {

          $query->orWhere('name', 'LIKE', "%" . $name . "%")
                ->orWhere('description', 'LIKE', "%" . $name . "%");
        });

	}

	public function user()
    {
        return $this->belongsTo('User');
    }
	
}