@extends("layouts.master") 

@section("content")
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail">
                <h4 class="categoryTitle">Contact us</h4>

                <h3>Phone: 01654687351</h3>

                <h3>Email: support@privateads.com</h3>
            </div>
        </div>
    </div>
</div>
@stop