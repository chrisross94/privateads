@extends("layouts.master") @section("content")

<div class="container-fluid">

    @include("ad/categories")

    <!-- random ads -->

    @include("ad/random", ["ads" => $ads])


    <!-- /random ads -->

    <!-- Sign up -->

    <div class="col-md-2 col-lg-2">
        <div class="panel panel-default">
            <div class="panel-body">
            <h4 class="categoryTitle" id="authUserName"></h4>
            <a href="#" id="logoutLink" style="display: none">Logout</a>
                <div id="userForm">

                    <h4 class="categoryTitle">Signup</h4>

                    <form method="post" action="user/register" class="form-inline" id="registerForm">
                        <fieldset>
                            <div class="row" style="padding-left: 5%;">

                                <div class="form-group">
                                    <input class="form-control" placeholder="First Name" required type="text" name="firstName">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Last Name" required type="text" name="lastName">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Location" required type="text" name="location">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Phone Number" required type="text" name="phoneNo">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" required type="text" name="username">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" required type="password" name="password">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" required type="email" name="email">
                                </div>
                                <br>

                                <div class="btn-group">
                                    <button class="btn btn-default" id="registerUser">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                    <p id="registerConfirmation" style="display: none">User registered successfully</p>

                    <h4 class="categoryTitle" style="padding-top: 5px;">Login</h4>

                    <form method="post" action="user/login" class="form-inline" id="loginForm">
                        <fieldset>
                            <div class="row" style="padding-left: 5%;">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" required="" type="text" name="username" id="loginUser">
                                </div>

                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" required="" type="password" name="password" id="loginPass">
                                </div>
                                <br>

                                <div class="btn-group">
                                    <button class="btn btn-default">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <p id="loginConfirmation" style="display: none">User logged in. Reloading in 3 seconds</p>
                    <h2 id="loginFail" style="display: none">Login failed</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- /signup -->
</div>
<!-- /container -->

@stop
