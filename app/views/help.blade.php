@extends("layouts.master")

@section("content")

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail">
                <h4 class="categoryTitle">Help</h4>

                <h3>Accessibility</h3><button class="btn btn-default" id=
                "accessible" type="button">Click here to enable accessibility
                mode</button>

                <h3>Usage</h3>

                <p>In order to use this website, firstly select an item on the
                homepage you are interested in, this can either by a category
                or an item in the featured or recent list. Clicking the view
                button will take you to that products respective page with more
                information about the item and details of the seller. On the
                category page, you can view all products in that particular
                category, and then click view to see more information</p>

                <h3>Data</h3>

                <p>We can collect data about you from this website. This includes
                <ul>
                  <li>IP address</li>
                  <li>Browser information</li>
                  <li>Other metadata</li>

                </ul>

                <p>By using this website you agree to us collecting this data and that it will be treated with the upmost privacy and security and will not be shared with third parties</p>

            </div>
        </div>
    </div>
</div>
@stop