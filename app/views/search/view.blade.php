@extends("layouts.master") 
@section("content")

<div class="container-fluid">
    <div class="col-md-12">
        <div class="thumbnail">
            <h4 class="categoryTitle">Showing results for: {{{$query}}}</h4> 
            @forelse($ads as $ad) 

            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="../uploads/{{{$ad->image}}}">

                        <div class="caption">
                            <h3>{{{$ad->name}}}</h3>

                            <p>{{{$ad->description}}}</p>

                            <p>{{ HTML::linkRoute('ads', "View", $ad->id, ["class" => "btn btn-primary"]) }}</p>
                        </div>
                    </div>
                </div>

                @empty
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">

                            <div class="caption">
                                <h3>Search returned 0 results</h3>

                                <p>We were unable to find any ads for this search</p>


                            </div>
                        </div>
                    </div>
                    @endforelse

                </div>
            </div>
        </div>
    </div>

    @stop
