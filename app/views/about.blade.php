@extends("layouts.master")

@section("content")

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail">
                <h4 class="categoryTitle">About Private Ads</h4>

                <p>Private Ads was founded in 1990 when a local entrepreneur
                identified there was an untapped market for private
                advertising. The first issue was published on March 12th 1990
                as a bi-weekly, four-page advertiser in Taunton, Somerset. The
                cost of publishing was offset through commercial advertising
                and this allowed private individuals to continually advertise
                their unwanted items free of charge.</p>

                <p>Today, the site has grown and adopted to the ever increasing
                popularity of the world wide web, and allows users to instantly
                publish ads on the site, and search through other ads users
                have posted. This means that Private Ads no longer needs to
                rely on publishing and can instead take advantage of the
                accessibility of the Internet</p>
            </div>
        </div>
    </div>
</div>
@stop