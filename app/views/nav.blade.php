<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo route('home');?>">Private Ads</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav" id="nav">
                <li id="home"><a class="" href="<?php echo route('home');?>">Home</a>
                </li>
                <li id="submit"><a class="" href="<?php echo route('submit')?>">Submit</a>
                </li>
                <li id="contact"><a class="" href="<?php echo route('contact');?>">Contact</a>
                </li>
                <li id="about"><a class="" href="<?php echo route('about')?>">About</a>
                </li>
                <li id="help"><a class="" href="<?php echo route('help')?>">Help</a>
                </li>
            </ul>
            <form class="navbar-form navbar-left" action="<?php echo route('search');?>" role="search" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="search" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <!--/.nav-collapse -->

    </div>
</nav>
