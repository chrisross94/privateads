<div class="col-md-2 col-lg-3">
    <div class="thumbnail">
        <h4 class="categoryTitle">Categories</h4>
            <div class="list-group">
        @foreach($categories as $category) 
        <a class="list-group-item" href="c/<?php echo strtolower($category->name)?>?id=<?php echo $category->id;?>">{{{$category->name}}}</a>
        @endforeach
        </div>
    </div>
</div>
