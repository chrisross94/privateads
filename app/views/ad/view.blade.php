@extends("layouts.master")

@section("content")

<div class="container">
    <div class="col-md-8">
        <div class="thumbnail">
            <h4 class="categoryTitle">Viewing Ad: {{{$ad->name}}}</h4>

            <div class="row">
                <div class="col-lg-12">
                    <div class="thumbnail">
                        <img src="../uploads/{{{$ad->image}}}">
                        <div class="caption">
                            <h3>Description</h3>

                            <p>{{{$ad->description}}}
                                <h3>Seller info</h3>

                                <p>Seller name: {{{$ad->user->firstName}}} {{{$ad->user->lastName}}}</p>

                                <p>Location: {{{$ad->user->location}}}</p>

                                <p>Phone: {{{$ad->user->phoneNo}}}</p>

                                <p>Price: {{{$ad->price}}}</p>

                                <p><a class="btn btn-primary" href="mailto:{{{$ad->user->firstName}}}@live.com?subject=I want to purchase your {{{$ad->name}}}">
                            Contact Seller</a>
                                </p>

                                @if(Auth::check()) 
                                    @if(Auth::user()->isAdmin)
                                     <p><a class="btn btn-danger" href="../delete/{{{$ad->id}}}">Delete</a></p>
                                    @endif 
                                @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
