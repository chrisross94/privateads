<div class="col-md-7">
    <div class="thumbnail">
        <h4 class="categoryTitle">Random ads</h4>
        <div class="row">

             @foreach ($ads as $ad)

            <div class="col-md-6">
                <div class="thumbnail">
                    <img src="uploads/{{{$ad->image}}}">

                    <div class="caption">
                        <h4>{{{$ad->name}}}</h4>

                        <p>{{{$ad->description}}}</p>

                        <p><a class="btn btn-primary" href="ad/{{{$ad->id}}}">View</a>
                        </p>
                    </div>
                </div>
            </div>
               @endforeach
        </div>
     


    </div>
</div>
