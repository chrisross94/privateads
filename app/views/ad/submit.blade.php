@extends("layouts.master") 
@section("content")

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="thumbnail">
                <h4 class="categoryTitle">Submit a new ad</h4>
                <form method="post" action="submit" enctype="multipart/form-data" id="submitAdForm">
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">Item Name</label>
                            <input class="form-control" name="name" placeholder="Enter the item name" type="text" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Item Description
                            </label>
                            <textarea class="form-control" name="description" required></textarea>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Item image</label>
                            <input type="file" required name="image">
                        </div>


                        <label class=" control-label">Item Price</label>

                        <div class="input-group">
                            <span class="input-group-addon">£</span>
                            <input class="form-control" name="price" required type="number"> <span class="input-group-addon">.00</span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Category</label>

                            <select class="form-control" name="category_id">
                            @foreach($categories as $category)
                                <option value="{{{$category->id}}}">{{{$category->name}}}</option>
                            @endforeach
                            </select>

                        </div>

                        <h3>Your Info</h3>

                        <div class="form-group">
                            <label class="control-label">Your Name</label>
                            <input class="form-control" name="username" placeholder="Your first and last name" required type="text" value="{{{Auth::user()->firstName}}}">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Phone Number</label>
                            <input class="form-control" name="phoneNo" placeholder="Enter your phone number" required type="tel" value="{{{Auth::user()->phoneNo}}}">
                        </div>
                        <input name="user_id" required type="hidden" value="{{{Auth::user()->id}}}">

                        <button class="btn btn-primary" name="submit">Submit</button>
                    </fieldset>
                </form>
                <h3 id="submitSuccess" style="display: none">Ad submitted successfully</h3>
                <h3 id="submitFail" style="display: none">Could not submit ad</h3>
            </div>
        </div>
    </div>
</div>

@stop
