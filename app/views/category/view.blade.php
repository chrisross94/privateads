@extends("layouts.master") 
@section("content")

<div class="container-fluid">
    <div class="col-md-12">
        <div class="thumbnail">
            <h4 class="categoryTitle">Viewing Ads in category: {{{$category}}}</h4> 
            @forelse($ads as $ad) 

            <div class="row">
                <div class="col-md-3">
                    <div class="thumbnail">
                       <img src="../uploads/{{{$ad->image}}}">

                        <div class="caption">
                            <h3>{{{$ad->name}}}</h3>

                            <p>{{{$ad->description}}}</p>

                            <p><a class="btn btn-primary" href="../ad/{{{$ad->id}}}">View</a>
                            </p>
                        </div>
                    </div>
                </div>

                @empty
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">

                            <div class="caption">
                                <h3>Category empty</h3>

                                <p>We were unable to find any ads for this category</p>


                            </div>
                        </div>
                    </div>
                    @endforelse

                </div>
            </div>
        </div>
    </div>

    @stop
