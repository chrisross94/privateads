<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post("search", ["as" => "search", "uses" => "SearchController@postIndex"]);
Route::get("ad/{id}", ["as" => "ads", "uses" =>"ViewAdController@showIndex"]);
Route::controller("c/{category}", "CController");
Route::controller("submit", "SubmitController");
Route::controller("user", "UserController");
Route::controller("delete/{id}", "DeleteController");
Route::post("submit", ["as" => "submit", "uses" => "SubmitController@postIndex"]);
Route::get("submit", ["as" => "submit", "uses" => "SubmitController@getIndex"]);
Route::get("about", ["as" => "about", "uses" => "AboutController@getIndex"]);
Route::get("help", ["as" => "help", "uses" => "HelpController@getIndex"]);
Route::get("contact", ["as" => "contact", "uses" => "ContactController@getIndex"]);
Route::get("/", ["as" => "home", "uses" => "HomeController@showIndex"]);
