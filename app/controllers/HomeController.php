<?php

class HomeController extends BaseController {

	public function showIndex()
	{

		$title = "Private Ads:: Home";

		$ads = Ad::orderByRaw("RAND()")->take(2)->get();
		
		return View::make('home')->with("title", $title)->with("ads", $ads);
	}

}