<?php

class DeleteController extends BaseController {

	public function getIndex($id) {
	
		$item = Ad::find($id);

		if(!$item) {
			return Redirect::route("home");
		}

		$item->delete();

		return Redirect::route("home");
	}


}