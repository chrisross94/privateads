<?php

class AboutController extends BaseController {

	public function getIndex()
	{
		$title = "Private Ads:: About";
		return View::make('about')->with("title", $title);
	}

}