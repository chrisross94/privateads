<?php

class ViewAdController extends BaseController {

	public function showIndex($id)
	{

		$title = "Private Ads:: Viewing ad";

		$ad = Ad::find($id);

		return View::make("ad/view")->with("title", $title)->with("ad", $ad);
	}

}