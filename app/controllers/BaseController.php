<?php

class BaseController extends Controller {


	function __construct() {
		
		$categories = Category::all();

		View::share("categories", $categories);

	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}