<?php

class ContactController extends BaseController {

	public function getIndex()
	{
		$title = "Private Ads:: Contact Us";
		return View::make('contact')->with("title", $title);
	}

}