<?php

class SubmitController extends BaseController {

	public function getIndex()
	{
		$title = "Private Ads: Submit";

		$categories = Category::all();

		if(!Auth::check()) {
			$title = "Submit: Not allowed";
			return View::make('ad/guest')->with("title", $title);
		} 

		return View::make('ad/submit')->with("title", $title)->with("categories", $categories);
	}

	public function postIndex() {

		$image = Input::file("image");
		$extension = $image->getClientOriginalExtension();
	    $name = str_random(5) . "." . $extension;
		$uploadDir = public_path() . "/uploads/";

		if($extension != "png" && $extension != "jpg" && $extension != "PNG" && $extension != "JPG") {
			return json_encode(["success" => false, "message" => "Invalid format. Only PNG or JPG are allowed. Your extension was " . $extension]);
		}

		$image->move($uploadDir, $name);

		$data = Input::only('name', 'description', 'price', 'category_id', "user_id");
		$data['image'] = $name;

		if(Ad::create($data)) {
			return json_encode(["success" => true, "message" => "Ad submitted successfully"]);
		} else {
			return json_encode(["success" => false, "message" => "Ad could not be submitted"]);
		}
	}


}