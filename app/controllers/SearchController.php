<?php

class SearchController extends BaseController {

	public function postIndex() {

		$name = Input::get("search");

		$ads = Ad::name($name)->get();
		
		$title = "Search results";
		return View::make("search/view")->with("title", $title)->with("ads", $ads)->with("query", $name);

	}


}