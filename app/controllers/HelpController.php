<?php

class HelpController extends BaseController {

	public function getIndex()
	{
		$title = "Private Ads:: Help";
		return View::make('help')->with("title", $title);
	}

}