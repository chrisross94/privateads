<?php

class UserController extends BaseController {

	public function getIndex()
	{
		//$title = "Private Ads: Submit";
		//return View::make('ad/submit')->with("title", $title);
	}

	public function getLogout() {
		Auth::logout();

		return Redirect::route("home");
	}

	public function postRegister() {


		$data = Input::only('username', 'password', 'email', "firstName", "lastName", "phoneNo", "location");
		
		$data['password'] = Hash::make($data['password']);

		try {
			User::create($data);
			return json_encode(["success" => true, "message" => "User created successfully"]);
		} catch (Exception $e) {
			return json_encode(["success" => false, "message" => "There was an error during registration process."]);
		}
		

	}

	public function postLogin() {

		$data = Input::only("username", "password");

		if(Auth::attempt(array("username" => $data['username'], "password" => $data['password']))) {
			return json_encode(["success" => true, "message" => "User authenticated successfully"]);
		} else {
			return json_encode(["success" => false, "message" => "User could not be authenticated"]);
		}

	}


} 