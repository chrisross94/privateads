<?php

class CController extends BaseController {

	public function getIndex($categoryName)
	{
		$title = "Private Ads:: Viewing Ads";

		$categoryID = Input::has("id") ? Input::get("id") : 1;

		//$category = Category::where("name", "=", $category)->get();
		$ads = Category::find($categoryID)->ads()->orderByRaw("RAND()")->take(3)->get();

		return View::make('category/view')->with("title", $title)->with("category", $categoryName)->with("ads", $ads);
	}

}